﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace lab7
{
    static class Program
    {
        static void Main(string[] args)
        {
            var path = args[0];
            if (PrintFiles(path))
            {
                Console.WriteLine();
                var oldestFile = (new DirectoryInfo(path)).GetOldestFile();
                if (oldestFile != null)
                {
                    Console.WriteLine("Oldest file: {0}", oldestFile);
                    CreateCollection(path);
                    Deserialize();
                    Console.ReadLine();
                }
            }
        }

        public static bool PrintFiles(string path)
        {
            Console.WriteLine(path);
            if (File.Exists(path)) ProcessFile(path, 0);

            else if (Directory.Exists(path)) ProcessDirectory(path, 0);

            else
            {
                Console.WriteLine("Invalid directory");
                return false;
            }
            return true;
        }

        public static void ProcessDirectory(string path, int depth)
        {
            var files = Directory.GetFiles(path);
            foreach (var file in files)
            {
                ProcessFile(file, depth);
            }

            var directories = Directory.GetDirectories(path);
            foreach (var directory in directories)
            {
                var pathParts = directory.Split("\\");
                string directoryName = pathParts[pathParts.Length - 1];
                for (int i = 0; i < depth; i++)
                {
                    Console.Write("       ");
                }
                var directoryInfo = new DirectoryInfo(directory);
                var directoryCreation = directoryInfo.GetOldestFile();
                var subFilesCount = (directoryInfo.GetDirectories().Length + directoryInfo.GetFiles().Length);
                Console.WriteLine("{0} ({1}) {2} {3}", directoryName, subFilesCount, directoryCreation, directoryInfo.GetRAHS());
                ProcessDirectory(directory, depth + 1);
            }
        }

        public static void ProcessFile(string path, int depth)
        {
            var pathParts = path.Split("\\");
            var fileName = pathParts[pathParts.Length - 1];

            for (var i = 0; i < depth; i++)
                Console.Write("    ");

            var fileInfo = new FileInfo(path);
            Console.WriteLine("{0} ({1} bytes) {2}", fileName, fileInfo.Length, fileInfo.GetRAHS());
        }

        public static DateTime? GetOldestFile(this DirectoryInfo directory)
        {
            DateTime? oldest = null;
            
            foreach (var directoryInfo in directory.GetDirectories())
            {
                var dirOldest = directoryInfo.GetOldestFile();

                if (oldest == null) oldest = dirOldest;

                else if (dirOldest < oldest) oldest = dirOldest;
            }
            foreach (var fileInfo in directory.GetFiles())
            {
                var creationDate = fileInfo.CreationTime;

                if (oldest == null) oldest = creationDate;

                else if (creationDate < oldest) oldest = creationDate;
            }

            return oldest;
        }

        public static string GetRAHS(this FileSystemInfo fileSystemInfo)
        {
            var output = "";

            var fileAttributes = fileSystemInfo.Attributes;

            if (fileAttributes == FileAttributes.ReadOnly) output += 'r';

            else output += '-';

            if (fileAttributes == FileAttributes.Archive) output += 'a';

            else output += '-';

            if (fileAttributes == FileAttributes.Hidden) output += 'h';

            else output += '-';

            if (fileAttributes == FileAttributes.System) output += 's';

            else output += '-';

            return output;
        }


        public static void CreateCollection(string path)
        {
            var collection = new SortedDictionary<string, int>(new StringComparer());
            if (File.Exists(path))
            {
                var file = new FileInfo(path);
                collection.Add(file.Name, (int) file.Length);
            }
            else if (Directory.Exists(path))
            {
                var dir = new DirectoryInfo(path);

                foreach (var subdir in dir.GetDirectories())
                    collection.Add(subdir.Name, (subdir.GetFiles().Length + subdir.GetDirectories().Length));

                foreach (var file in dir.GetFiles())
                    collection.Add(file.Name, (int)file.Length);
            }
            var fs = new FileStream("DataFile.dat", FileMode.Create);
            var formatter = new BinaryFormatter();
            try
            {   
                formatter.Serialize(fs, collection);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Serialization error: {0}", e.Message);
            }
            fs.Close();
        }

        public static void Deserialize()
        {
            var collection = new SortedDictionary<string, int>(new StringComparer());
            var fs = new FileStream("DataFile.dat", FileMode.Open);
            try
            {
                var formatter = new BinaryFormatter();
                collection = (SortedDictionary<string, int>)formatter.Deserialize(fs);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Serialization error: {0}", e.Message);
            }

            foreach(var file in collection)
                Console.WriteLine("{0} -> {1}", file.Key, file.Value);
        }
    }   
}
